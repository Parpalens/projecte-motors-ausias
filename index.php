<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ausias Motors</title>
	<link rel="stylesheet" type="text/css" href="css/input.css" />
	<script src="https://code.jquery.com/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="js/matricula.js"></script>


  <!-- ........................................... BARRA MENÚ ........................................................ -->

  	 <?php include('plantilles/header.html') ?>

  <!-- ........................................... CONTAINER SLIDER ........................................................ -->
   <div class="container">

  	<input type="radio" id="i1" name="images" checked />
  	<input type="radio" id="i2" name="images" />
  	<input type="radio" id="i3" name="images" />
  	<input type="radio" id="i4" name="images" />
  	<input type="radio" id="i5" name="images" />


  <!-- ........................................... IMG 1 ........................................................ -->

  	<div class="slide_img" id="one">
  			<img src="img/slide-1.jpg">
  	</div>

  <!-- ........................................... IMG 2 ........................................................ -->

  	<div class="slide_img" id="two">
  			<img src="img/slide-2.png">
  	</div>

  <!-- ........................................... IMG 3 ........................................................ -->


  	<div class="slide_img" id="three">
  			<img src="img/slide-3.jpg">
  	</div>

  <!-- ........................................... IMG 4 ........................................................ -->

  	<div class="slide_img" id="four">
  			<img src="img/slide-4.jpg">
	</div>

  <!-- ........................................... IMG 5 ........................................................ -->


  	<div class="slide_img" id="five">
  			<img src="img/slide-5.jpg">
  	</div>

  <!-- ........................................... MATRICULA ........................................................ -->

  	 <div class="matricula">
  		<form action="comprovamatr.php" method="GET">
  			<input id="inputmatricula" type="text" name="matricula" maxlength="7" size="25" placeholder="Matricula..." autofocus >
  			<button class="submit_matricula" type="submit">Continuar</button>
  		</form>
  	</div>

  <!-- ........................................... SLIDER ........................................................ -->


  	<div id="nav_slide">
  		<label for="i1" class="dots" id="dot1"></label>
  		<label for="i2" class="dots" id="dot2"></label>
  		<label for="i3" class="dots" id="dot3"></label>
  		<label for="i4" class="dots" id="dot4"></label>
  		<label for="i5" class="dots" id="dot5"></label>
  	</div>

  </div>
  <div class="espacioenblanco"></div>
    <div class="espacioenblanco"></div>

<!-- ........................................... FOOTER ........................................................ -->

<footer style="position: absolute;bottom: 0px;">
    <div id="menu-footer">
        <ul class="footer-menu">
            <li><a href="index.php">Inici</a></li>
            <li><a href="about.php">Sobre Nosaltres</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>Copyright©</p>
        <p>Designed by Grup4</p>
    </div>

</footer>

<!--------------------------------------------------------------------------------------->


</body>

</html>
