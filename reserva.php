<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cita previa</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script type="text/javascript" src="js/calendar.js"></script>
    <script type="text/javascript" src="js/calendari.js"></script>
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/reserva.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/spaces.css">
    <link rel="stylesheet" href="css/slider.css">

</head>
<body>

<?php
	$matricula = $_COOKIE["matricula"];

?>
<!-- ........................................... BARRA MENÚ ........................................................ -->

	 <?php include('plantilles/header.html');  ?>

<!-- ........................................... CONTINGUT RESERVA........................................................ -->
	<p id='infomat'>La teva matricula és: <span id="mat"><?php  echo strtoupper($matricula);?></span></p>
	<br>

	<div align="center">
		<div id="data" align="center">
			<p id="fuente">Escull el dia de la visita:</p>
			<button id="prevmes" type='button'>◄</button>
			<span id="mes"></span>
			<button id="proxmes" type='button'>►</button>
			<p></p>
			<table id="calendari"></table>
		</div>
	</div>
	<br>
	<div id="seguentres"></div>
	<br><br>

<!-- ........................................... FOOTER ........................................................ -->

	 <?php include('plantilles/footer.html') ?>

<!-- ........................................................................................................... -->

</body>
</html>
