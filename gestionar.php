<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ausias Motors</title>
  
</head>
<body>

<!-- ........................................... BARRA MENÚ ........................................................ -->

	 <?php include('plantilles/header.html') ?> 

<!-- ........................................... INFO VÀRIA........................................................ -->

	<br> <br>
	<?php
	
		$matricula = $_COOKIE['matricula'];
		echo '<h2 align="center"> Ja existeix un registre amb la matricula ' . $matricula . ', desitja modificar o esborrar la seva cita? </h2>';
	?>
	
	<div id="informacio">
		<?php
//......................................................................................................................

			//MySQL Database Connect
			include 'login_php.php';
//......................................................................................................................
		
			$sql = "SELECT * from reserves where matricula='$matricula'";
		
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					
					$matricula = $row['matricula'];
					$nom = $row['nom'];
					$email = $row['email'];
					$telefon = $row['telefon'];
					$data= $row['data'];
					$gestio=1;
					$data_dia = strftime("%Y-%m-%d", strtotime($row['data']));
					$data_hora = strftime("%H:%M", strtotime($row['data']));
					
		?>
					<p id="info-matr">
						<?php
							echo "Matricula: " . $matricula . "<br> <br> Nom: " . $nom . "<br> <br> Email: " . $email . "<br> <br> Telèfon: " . $telefon . "<br> <br> Data: " . $data;
						?>
					</p>
		<?php
				}
			} else {
				echo "0 results";
			}
			$conn->close();
		?>
	</div>
	
	
	<div class="gestionar">
		<form action="reserva.php" method="POST">
			Matricula : <input type="text" name="matricula" value=<?php echo $matricula ?>>
			Data : <input type="date" name="data" value="<?php echo $data_dia ?>">
			Hora: <input type="time" name="hora" value="<?php echo $data_hora ?>">
			
			<input type="submit" value="Modificar">
		</form>
		<br />
		
		
		<form action="esborrar.php" method="POST">
			Matricula : <input type="text" name="matricula" value="<?php echo $matricula ?>">
			Data : <input type="date" name="data" value="<?php echo $data_dia ?>">
			Hora: <input type="time" name="hora"  value="<?php echo $data_hora ?>">
			<input type="submit" value="Esborrar">
		</form>
	</div>
	
	
	

<!-- ........................................... FOOTER ........................................................ -->

	 <?php include('plantilles/footer.html') ?> 

<!-- ............................................................................................................ -->

  
</body>
</html>

