$(document).ready(function(){
	$("input[type=text]").keyup(function(){
        var matricula =$(this).val();
		var matriculaCorrecta=/^\d{4}[a-zA-Z]{3}$/;
        if(matricula.length== 0){
			$("#inputmatricula").css("background-color","white")		
		}
		else if(matricula.length > 0 && matricula.length < 7){
			$("#inputmatricula").css("background-color", "#FFB060");
		}
		else{
			if(matriculaCorrecta.test(matricula)==true){
				$("#inputmatricula").css("background-color", "#6DB52D");
			}
			else{
				$("#inputmatricula").css("background-color", "#FF9A9A");
			}
		}
    });
});   
