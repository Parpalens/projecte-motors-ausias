window.onload = function() {

    function imprimirCalendario(){

        //Variable que almacena el id del div donde almacenaremos el calendario para facilitar acceso
        var calendarioid = document.getElementById("calendari");
        //Variable que almacena todo el contenido a insertar en el div del HTML
        var contenidocalendario = "";

        //funcion que añade a contenidocalendario la cabecera del calendario
        function cabecera(){
            contenidocalendario += "<tr style='background-color:#FF9B21;'><th> DILL </th>" + "<th> DIM </th>" + "<th> DIX </th>" + "<th> DIJ </th>" + "<th> DIV </th>" + "<th> DIS </th>" + "<th> DIU </th></tr>";
        }

        function mesDias(year, month) {
            month++;
            return new Date(year, month, 0).getDate();
        }

        //funcion que añade a contenidocalendario las celdas vacias
        function celdasvacias(num){
            for (var i = 0; i < num; i++) {
                contenidocalendario += "<th> </th>";
            }
        }

        function rellenaCalendario(año, mes) {

            var diasMes = mesDias(año, mes);
            var dia = 1;

            var fecha = new Date(año, mes, 1);
            mesDias = fecha.getDay();

            for (var i = 0; i < 6; i++) {
                contenidocalendario += "<tr>";
                for (var j = 0; j < 7; j++) {
                    if ((i === 0) && (j === 0)) {

                        switch (mesDias) {
                            case 0:
                                celdasvacias(6);
                                j += 6
                                break;
                            case 1:
                                break;
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                celdasvacias(mesDias-1);
                                j += mesDias - 1;
                                break;
                        }
                    }
                    if (dia <= diasMes) {
                        mes++;
                        var hoy             = new Date();
                        var fechaFormulario = new Date(año + "-" + mes + "-" + dia);
                        mes--;

// Comparamos solo las fechas => no las horas!!
                        hoy.setHours(0,0,0,0);  // Lo iniciamos a 00:00 horas
                        var dt = new Date(año, mes, dia);
                        if (dt.getDay()==6 || dt.getDay()===0){
                            contenidocalendario += "<th id='" + dia + "' class='finde'>" + dia + "</th>";
                        }

                        else{

                            if (hoy <= fechaFormulario) {
                                console.log("Fecha a partir de hoy");
                                contenidocalendario += "<td id='" + dia + "' class='" + mes + " " + año + "'>" + dia + "</td>";
                            }
                            else {
                                console.log("Fecha pasado");
                                contenidocalendario += "<th id='" + dia + "' class='" + mes + " pasado " + año + "'>" + dia + "</th>";
                            }
                        }

                        dia++;

                    } else {
                        celdasvacias(7-j);//Funcion para hacer celdas vacias
                        j = 10;
                        i = 10;
                    }
                } // fin segundo for
                contenidocalendario += "</tr>";
            } // fin primer for
            calendarioid.innerHTML = contenidocalendario;
        } // fin function

        //llamo a funcion cabecera
        cabecera();
        //llamo a funcion rellenacalendario
        rellenaCalendario(anyusuari, mesusuari);

        //Envio al HTML el contenido de la tabla
        calendarioid.innerHTML = contenidocalendario;

    }//Fin imprimircalendario


//Variables para introducir la fecha
    var f = new Date();
    var diausuari = f.getDate();
    var mesusuari = f.getMonth();
    var anyusuari = f.getFullYear();

//Poner el nombre del mes en la parte superior del calendario
    if (mesusuari===0) document.getElementById("mes").innerHTML = "Gener de " + anyusuari;
    else if (mesusuari==1) document.getElementById("mes").innerHTML = "Febrer de " + anyusuari;
    else if (mesusuari==2) document.getElementById("mes").innerHTML = "Març de " + anyusuari;
    else if (mesusuari==3) document.getElementById("mes").innerHTML = "Abril de " + anyusuari;
    else if (mesusuari==4) document.getElementById("mes").innerHTML = "Maig de " + anyusuari;
    else if (mesusuari==5) document.getElementById("mes").innerHTML = "Juny de " + anyusuari;
    else if (mesusuari==6) document.getElementById("mes").innerHTML = "Juliol de " + anyusuari;
    else if (mesusuari==7) document.getElementById("mes").innerHTML = "Agost de " + anyusuari;
    else if (mesusuari==8) document.getElementById("mes").innerHTML = "Septembre de " + anyusuari;
    else if (mesusuari==9) document.getElementById("mes").innerHTML = "Octubre de " + anyusuari;
    else if (mesusuari==10) document.getElementById("mes").innerHTML = "Novembre de " + anyusuari;
    else document.getElementById("mes").innerHTML = "Desembre de " + anyusuari;


//Ejecuto funcion imprimirCalendario
    imprimirCalendario();

    document.getElementById("prevmes").onclick = function() {cambiamenys()};
    document.getElementById("proxmes").onclick = function() {cambiames()};
    $("#prevmes").toggleClass("prmes");



    function cambiamenys() {
        if (mesusuari>0){
            mesusuari--;
        }
        else{
            mesusuari+=11;
            anyusuari--;
        }
        if ((mesusuari+1<=(f.getMonth()+1) && anyusuari==f.getFullYear()) || anyusuari<f.getFullYear()){
            $("#prevmes").toggleClass("prmes");
        }


        //Poner el nombre del mes en la parte superior del calendario
        if (mesusuari===0) document.getElementById("mes").innerHTML = "Gener de " + anyusuari;
        else if (mesusuari==1) document.getElementById("mes").innerHTML = "Febrer de " + anyusuari;
        else if (mesusuari==2) document.getElementById("mes").innerHTML = "Març de " + anyusuari;
        else if (mesusuari==3) document.getElementById("mes").innerHTML = "Abril de " + anyusuari;
        else if (mesusuari==4) document.getElementById("mes").innerHTML = "Maig de " + anyusuari;
        else if (mesusuari==5) document.getElementById("mes").innerHTML = "Juny de " + anyusuari;
        else if (mesusuari==6) document.getElementById("mes").innerHTML = "Juliol de " + anyusuari;
        else if (mesusuari==7) document.getElementById("mes").innerHTML = "Agost de " + anyusuari;
        else if (mesusuari==8) document.getElementById("mes").innerHTML = "Septembre de " + anyusuari;
        else if (mesusuari==9) document.getElementById("mes").innerHTML = "Octubre de " + anyusuari;
        else if (mesusuari==10) document.getElementById("mes").innerHTML = "Novembre de " + anyusuari;
        else document.getElementById("mes").innerHTML = "Desembre de " + anyusuari;
        imprimirCalendario();

    }

    function cambiames() {
        if (mesusuari<11){
            mesusuari++;
        }
        else{
            mesusuari-=11;
            anyusuari++;
        }

        $("#prevmes").removeClass("prmes");

        //Poner el nombre del mes en la parte superior del calendario
        if (mesusuari===0) document.getElementById("mes").innerHTML = "Gener de " + anyusuari;
        else if (mesusuari==1) document.getElementById("mes").innerHTML = "Febrer de " + anyusuari;
        else if (mesusuari==2) document.getElementById("mes").innerHTML = "Març de " + anyusuari;
        else if (mesusuari==3) document.getElementById("mes").innerHTML = "Abril de " + anyusuari;
        else if (mesusuari==4) document.getElementById("mes").innerHTML = "Maig de " + anyusuari;
        else if (mesusuari==5) document.getElementById("mes").innerHTML = "Juny de " + anyusuari;
        else if (mesusuari==6) document.getElementById("mes").innerHTML = "Juliol de " + anyusuari;
        else if (mesusuari==7) document.getElementById("mes").innerHTML = "Agost de " + anyusuari;
        else if (mesusuari==8) document.getElementById("mes").innerHTML = "Septembre de " + anyusuari;
        else if (mesusuari==9) document.getElementById("mes").innerHTML = "Octubre de " + anyusuari;
        else if (mesusuari==10) document.getElementById("mes").innerHTML = "Novembre de " + anyusuari;
        else document.getElementById("mes").innerHTML = "Desembre de " + anyusuari;
        imprimirCalendario();

    }



};
