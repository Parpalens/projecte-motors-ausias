var x;
x=$(document);
x.ready(agafacalendari);

function agafacalendari()
{
    var id, mesclass, anyclass;
    $("#calendari").on('click','td', function(id) {

        $(".highlight").css("background-color", "white");
        $(".highlight").css("color", "black");
        id = parseInt($(this).attr("id"));
        mesclass = parseInt($(this).attr("class"));
        anyclass = parseInt($(this).attr("class").split(' ')[1]);
        appareixbotoseguent(id, mesclass, anyclass);
        $(this).css("background-color", "#62639A");
        $(this).css("color", "white");
        $(this).addClass( "highlight" );
    });

    $("#calendari").on('click','.pasado', function(id) {

        warning();
        $(".highlight").css("background-color", "white");
        $(".highlight").css("color", "black");

    });

    $("#calendari").on('mouseover','td', function() {
        $(this).addClass( "hovered" );
    });

    $("#calendari").on('mouseleave','td', function() {
        $(this).removeClass ("hovered");
    });
}

function appareixbotoseguent(id, mesclass, anyclass){
    var divboto;
    divboto = $("#seguentres");

    document.cookie = "cookieDia=" + id + ";";
    document.cookie = "cookieMes=" + (mesclass+1) + ";";
    document.cookie = "cookieAny=" + anyclass + ";";
    divboto.html('<form action="reserva_hora.php" method="get"><button id="submitcalendar" type="submit">Hora</button></form>');
}

function warning(){
    var divboto;
    divboto = $("#seguentres");
    divboto.html('<h4>No pots escollir una data anterior a avui!</h4>');
}
