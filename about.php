<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ausias Motors</title>
</head>
<body>

<!-- ........................................... BARRA MENÚ ........................................................ -->

	<?php include('plantilles/header.html') ?>

 <!-- ........................................... INFO VÀRIA ........................................................ -->

	<img id="horari" src="img/foto1.png" height="250px" width="500px">
	<div id="fullwidth">
		<div id="primer" class="lila floatleft">
			<h5 class="dentrodecuadro">Efectivitat</h5>
			<img class="dentrodecuadro imagendecuadro" src="img/eficacia.png" alt="">
			<p class="dentrodecuadro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis a, aut voluptatum aliquam veritatis deserunt eum voluptates ex beatae minima ratione, accusamus, dignissimos qui. Vel quo aperiam, quas rerum dicta!</p>
		</div>
		<div id="segundo" class="naranja floatleft">
			<h5 class="dentrodecuadro">Profesionalitat</h5>
			<img class="dentrodecuadro imagendecuadro" src="img/profesionalidad.png" alt="">
			<p class="dentrodecuadro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis a, aut voluptatum aliquam veritatis deserunt eum voluptates ex beatae minima ratione, accusamus, dignissimos qui. Vel quo aperiam, quas rerum dicta!</p>
		</div>
		<div id="tercero" class="lila floatleft">
			<h5 class="dentrodecuadro">La millor relacio Qualitat-Preu</h5>
			<img class="dentrodecuadro imagendecuadro" src="img/calidad.png" alt="">
			<p class="dentrodecuadro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis a, aut voluptatum aliquam veritatis deserunt eum voluptates ex beatae minima ratione, accusamus, dignissimos qui. Vel quo aperiam, quas rerum dicta!</p>

		</div>
		<div id="cuarto" class="naranja floatleft">
			<h5 class="dentrodecuadro">Liders al sector</h5>
			<img class="dentrodecuadro imagendecuadro" src="img/leader.png" alt="">
			<p class="dentrodecuadro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis a, aut voluptatum aliquam veritatis deserunt eum voluptates ex beatae minima ratione, accusamus, dignissimos qui. Vel quo aperiam, quas rerum dicta!</p>
		</div>
	</div>
	<div>
  	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.3585380279633!2d2.1045620151123368!3d41.38801940391943!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a498530e78a55b%3A0xbd7d7a5e45217c20!2sAv.+d&#39;Esplugues%2C+Barcelona!5e0!3m2!1sca!2ses!4v1512041081774" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>



  <!-- ........................................... FOOTER ........................................................ -->

   <?php include('plantilles/footer.html') ?>

<!-- ........................................................................................................... -->

</body>
</html>
