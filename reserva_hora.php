<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Hora</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script type="text/javascript" src="js/hores.js"></script>
  <link rel="stylesheet" href="css/footer.css">
	<link rel="stylesheet" href="css/reserva.css">
	<link rel="stylesheet" href="css/nav.css">
	<link rel="stylesheet" href="css/spaces.css">
	<link rel="stylesheet" href="css/slider.css">

</head>
<body>

<?php
    $matricula = $_COOKIE["matricula"];
?>


<!-- ........................................... BARRA MENÚ ........................................................ -->


  <?php include('plantilles/header.html') ?>

<!-- ........................................... CONTINGUT RESERVA........................................................ -->

<!-- ........................................... CONNEXIÓ BBDD........................................................ -->
		<?php //MySQL Database Connect
		include 'login_php.php'; ?>
<!-- ........................................... INICIEM LA CRIDA DE PARAMETRES ...................................... -->

	<?php

		$sql = "SELECT * from reserves where cont=2";
		$result = $conn->query($sql);

		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				echo strftime("%H%M", strtotime($row['data']));
			}
		}
		else echo "0 results";
	?>


	<p id="infomat">La teva matricula és: <?php  echo $matricula; ?></p>
	<br>
    <div id="divhora">
        <p id="fuente">Escull la hora:</p>
        <div id="taulahores"><table id="hora"></table></div>
    </div>

	<br>
	<div id="seguentres">
	</div>
	<br><br>

  <!-- ........................................... FOOTER ........................................................ -->

  	 <?php include('plantilles/footer.html') ?>

  <!-- ........................................................................................................... -->

  </body>
  </html>
